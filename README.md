Yet another one page scroller
============

Yes, another one
This is just simple scroller of your content.

this is mirror of [github](https://github.com/ezhikov/pagescroller "github") repo

Usage
------
Your HTML should be like this:

```html
<article>
    <section></section>
    <section></section>
    ...
    <section></section>
</article>
```

And your js is simply like this (do not forget to include jQuery):

```javascript
$('article').yaops(options)
```

Navigation
______
You can use `data` attribute to navigate. For example:

```html
<a href="#" data-index=3>Third slide</a>
```

or

```html
<span data-index=5>Fifth slide</span>
```

Note that indexes in navigation calculating from 1

Options
------
By this time we have only this options:


| Option | Default  | Description |
|--------|:--------:|-------------|
| items | Container direct children | need if blocks is not direct children of your container. Should use jquery object.
| easing | `"swing"` | if you want use more easings, you should include easing plugin or something like that. |
| keys | `false` |  boolean which define to use keyboard for scrolling or not (Page Up, Page Down and arrows).
| speed | `600` | speed of slide animation.

Callbacks
------
There is two callbacks:

`beforeScroll` and `afterScroll`
They get *next* slide index.

You can use them like this:

```javascript
$('article').yaops(beforeScroll: function(index){ alert(index) })
```

Mobile browsers
------
Not work in mobile browsers. I'll fix this soon.
