/**
 * Yet Another One Page Scroller 1.3.0
 * https://github.com/ezhikov/pagescroller
 * MIT licensed
 *
 * Copyright (C) Kirill Popolov aka ezhikov
 */

(function ($) {
    // plugin
    $.fn.yaops = function (options) {
        var $container = this,
            settings = $.extend({
                items: $container.children(),
                easing: "swing",
                keys: false,
                speed: 600,
                beforeScroll: null,
                afterScroll: null
            }, options),
            changeSlide = function (delta, nextIndex) {
				
                if ($container.filter(':animated').length < 1) {
                    var currentIndex = settings.items.index(settings.items.filter('.active')),
                        nextIndex = typeof nextIndex !== "undefined" ? nextIndex : currentIndex - delta,
                        pos;
                    if (nextIndex > -1 && nextIndex < settings.items.length) {
                        $.isFunction(settings.beforeScroll) && settings.beforeScroll.call(this, nextIndex);
                        pos = settings.items
                            .removeClass('active')
                            .eq(nextIndex)
                            .addClass('active')
                            .position().top;
                        $container.animate({
                            top: -pos
                        },
                            settings.speed,
                            settings.easing,
                            $.isFunction(settings.afterScroll) && settings.afterScroll.call(this, nextIndex));
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            },
            MouseWheelHandler = function (e) {
                var event = window.event || e, // old IE support
                    delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
                changeSlide(delta);
                return false;
            };

        settings.items.css({
            'position': 'relative'
        }).eq(0).addClass('active');
        $container.css('position', 'relative');
        $('body').css('overflow', 'hidden');

        //navigation handlers
        $('[data-index]').click(function(){
            var nextIndex = parseInt($(this).data('index'))-1;
            changeSlide(0, nextIndex);
            return false;
        })
        
        //define mousewheel event
        if (document.addEventListener) {
            // IE9, Chrome, Safari, Opera
            document.addEventListener("mousewheel", MouseWheelHandler, false);
            // Firefox
            document.addEventListener("DOMMouseScroll", MouseWheelHandler, false);
        } else {
            // IE 6/7/8
            document.attachEvent("onmousewheel", MouseWheelHandler);
        }

        if (settings.keys === true) {
            $(document).keydown(function (e) {
                var code = e.keyCode || e.which,
                    delta;
                if (code === 33 || code === 38) {
                    delta = 1;
                } else if (code === 34 || code === 40) {
                    delta = -1;
                } else {
                    return;
                }
                changeSlide(delta);
                return false;
            });
        }
        
        return this;
    };
}(jQuery));